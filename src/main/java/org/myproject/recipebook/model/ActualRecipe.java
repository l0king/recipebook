package org.myproject.recipebook.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;


@Document(collection = "actualrecipe")
public class ActualRecipe {
    @Id
    private String id;
    private String name;
    private String description;
    private String body;
    private LocalDateTime createDate;
    private LocalDateTime modifyDate;

    public Object getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public ActualRecipe() {
    }

    public ActualRecipe(String name, String description, String recipe, LocalDateTime createDate, LocalDateTime modifyDate) {
        this.name = name;
        this.description = description;
        this.body = recipe;
        this.createDate = createDate;
        this.modifyDate = modifyDate;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public LocalDateTime getModifyDate() {
        return modifyDate;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public void setModifyDate(LocalDateTime modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }
}
