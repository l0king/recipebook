package org.myproject.recipebook.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;


@Document(collection = "oldrecipe")
public class OldRecipe {
    @Id
    private String id;
    private Object newRecipeId;
    private String name;
    private String description;
    private String body;
    private LocalDateTime createDate;
    private LocalDateTime modifyDate;

    public OldRecipe() {
    }

    public OldRecipe(ActualRecipe actualRecipe, LocalDateTime modifyDate) {
        this.newRecipeId = actualRecipe.getId();
        this.name = actualRecipe.getName();
        this.description = actualRecipe.getDescription();
        this.createDate = actualRecipe.getCreateDate();
        this.modifyDate = modifyDate;
        this.body = actualRecipe.getBody();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getNewRecipeId() {
        return newRecipeId;
    }

    public void setNewRecipeId(Object newRecipeId) {
        this.newRecipeId = newRecipeId;
    }


    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(LocalDateTime modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
