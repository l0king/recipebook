package org.myproject.recipebook.rest.controllers;

import java.time.LocalDateTime;
import java.util.List;

import org.myproject.recipebook.model.ActualRecipe;
import org.myproject.recipebook.model.OldRecipe;
import org.myproject.recipebook.repository.ActualRecipeRepository;
import org.myproject.recipebook.repository.OldRecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/recipe")
public class RestRecipeController{
    @Autowired
    private ActualRecipeRepository actualRecipeRepository;
    @Autowired
    private OldRecipeRepository oldRecipeRepository;

    @GetMapping(path = "/", produces = "application/json")
    public List<ActualRecipe> getAll() {
        return (List<ActualRecipe>) actualRecipeRepository.findAll();
    }

    @PostMapping(path = "/")
    public ActualRecipe addNewRecipe(@RequestBody ActualRecipe recipe) {
        recipe.setCreateDate(LocalDateTime.now());
        return actualRecipeRepository.save(recipe);
    }

    @GetMapping(path = "/{id}", produces = "application/json")
    public ActualRecipe getRecipe(@PathVariable String id) {
        return actualRecipeRepository.findById(id);
    }

    @PutMapping(path = "/{id}", produces = "application/json") 
    public ActualRecipe ediRecipe(@RequestBody ActualRecipe recipe, @PathVariable String id) {
        LocalDateTime editDateTime = LocalDateTime.now();

        ActualRecipe nonActual = actualRecipeRepository.findById(id);
        OldRecipe oldRecipe = new OldRecipe(nonActual, editDateTime);
        oldRecipeRepository.save(oldRecipe);
        recipe.setModifyDate(editDateTime);
        recipe.setCreateDate(oldRecipe.getCreateDate());
        recipe.setId(id);
        return actualRecipeRepository.save(recipe);
    }

    @GetMapping(path = "/{id}/history", produces = "application/json")
    public List<OldRecipe> history(@PathVariable String id) {
        return oldRecipeRepository.findByNewRecipeId(id);
    }

    @GetMapping("/{id}/history/{oldRecipeId}")
    public OldRecipe showOldRecipe(@PathVariable String oldRecipeId) {
        return oldRecipeRepository.findById(oldRecipeId).orElseThrow();
    }
}