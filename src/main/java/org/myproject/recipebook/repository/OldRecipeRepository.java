package org.myproject.recipebook.repository;

import org.myproject.recipebook.model.OldRecipe;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface OldRecipeRepository extends CrudRepository<OldRecipe, Integer> {
    Optional<OldRecipe> findById(String id);

    List<OldRecipe> findByNewRecipeId(String id);
}
