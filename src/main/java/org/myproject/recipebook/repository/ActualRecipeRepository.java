package org.myproject.recipebook.repository;

import org.myproject.recipebook.model.ActualRecipe;
import org.springframework.data.repository.CrudRepository;

public interface ActualRecipeRepository extends CrudRepository<ActualRecipe, Integer> {
    ActualRecipe findById(String id);
}
