package org.myproject.recipebook.controllers;

import org.myproject.recipebook.model.ActualRecipe;
import org.myproject.recipebook.model.OldRecipe;
import org.myproject.recipebook.repository.ActualRecipeRepository;
import org.myproject.recipebook.repository.OldRecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping("/recipe")
public class RecipeController {
    @Autowired
    private ActualRecipeRepository actualRecipeRepository;
    @Autowired
    private OldRecipeRepository oldRecipeRepository;


    @GetMapping("/all")
    public String getAll(Model model) {
        List<ActualRecipe> list = (List<ActualRecipe>) actualRecipeRepository.findAll();
        model.addAttribute("recipes", list);
        return "all";
    }

    @PostMapping("/add")
    public String addNewRecipe(@ModelAttribute ActualRecipe recipe) {
        recipe.setCreateDate(LocalDateTime.now());
        actualRecipeRepository.save(recipe);
        return "redirect:/recipe/all";
    }


    @GetMapping("/add")
    public String showAddForm(@ModelAttribute ActualRecipe recipe, Model model) {
        model.addAttribute("recipe", recipe);
        return "add";
    }

    @GetMapping("/{id}")
    public String getRecipe(@PathVariable String id, Model model) {
        ActualRecipe recipe = actualRecipeRepository.findById(id);
        model.addAttribute("recipe", recipe);
        return "recipe";
    }

    @GetMapping("/{id}/edit")
    public String editRecipe(@PathVariable String id, Model model) {
        ActualRecipe recipe = actualRecipeRepository.findById(id);
        model.addAttribute("recipe", recipe);
        return "edit";
    }

    @PostMapping("/{id}/edit")
    public String doEditRecipe(@PathVariable String id, ActualRecipe recipe) {
        LocalDateTime editDateTime = LocalDateTime.now();

        ActualRecipe nonActual = actualRecipeRepository.findById(id);
        OldRecipe oldRecipe = new OldRecipe(nonActual, editDateTime);
        oldRecipeRepository.save(oldRecipe);
        recipe.setModifyDate(editDateTime);
        recipe.setCreateDate(oldRecipe.getCreateDate());
        recipe.setId(id);
        actualRecipeRepository.save(recipe);
        return "redirect:/recipe/" + id;
    }

    @GetMapping("/{id}/history")
    public String history(@PathVariable String id, Model model) {
        List<OldRecipe> oldRecipes = oldRecipeRepository.findByNewRecipeId(id);
        model.addAttribute("oldrecipes", oldRecipes);
        return "recipehistory";
    }

    @GetMapping("/{id}/history/{oldRecipeId}")
    public String showOldRecipe(@PathVariable String oldRecipeId, Model model) {
        OldRecipe oldRecipe = oldRecipeRepository.findById(oldRecipeId).orElseThrow();
        model.addAttribute("oldrecipe", oldRecipe);
        return "oldrecipe";
    }
}

